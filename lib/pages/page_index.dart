import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '등산 사랑회 소개',
        ),
        centerTitle: true,
        elevation: 0.0,
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.search),
          ),
        ],
      ),
      drawer: Drawer(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset('assets/mountain.jpg'),
            Container(
              width: 400,
              height: 100,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [
                    Colors.red,
                    Colors.yellow,
                  ],
                ),
              ),
              child: Column(
                children: [
                  Text(
                    '등산 사랑회',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                  Text(
                    '회비: 10,000',
                    style: TextStyle(
                      fontSize: 10,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    '매주 토요일 새벽 6시 출발',
                    style: TextStyle(fontSize: 15),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(30),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        '주요 멤버',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: 300,
                          height: 200,
                          alignment: Alignment.topLeft,
                          child: Row(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    margin: EdgeInsets.fromLTRB(0, 10, 40, 5),
                                    child: Column(
                                      children: [
                                        Image.asset(
                                          'assets/smiley.png',
                                          width: 120,
                                          height: 120,
                                        ),
                                        Text('회장: 홍길동'),
                                        Text('')
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.fromLTRB(10, 10, 0, 5),
                                    child: Column(
                                      children: [
                                        Image.asset(
                                          'assets/sad.png',
                                          width: 120,
                                          height: 120,
                                        ),
                                        Text('부회장: 윤길동'),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: 300,
                          height: 200,
                          alignment: Alignment.topLeft,
                          child: Row(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    margin: EdgeInsets.fromLTRB(0, 10, 40, 5),
                                    child: Column(
                                      children: [
                                        Image.asset(
                                          'assets/money.png',
                                          width: 120,
                                          height: 120,
                                        ),
                                        Text('부원: 강길동'),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.fromLTRB(10, 10, 0, 5),
                                    child: Column(
                                      children: [
                                        Image.asset(
                                          'assets/sad.png',
                                          width: 120,
                                          height: 120,
                                        ),
                                        Text('부원: 소길동'),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ))
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: 300,
                          height: 200,
                          alignment: Alignment.topLeft,
                          child: Row(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    margin: EdgeInsets.fromLTRB(0, 10, 40, 5),
                                    child: Column(
                                      children: [
                                        Image.asset(
                                          'assets/oo.png',
                                          width: 120,
                                          height: 120,
                                        ),
                                        Text('부원: 하길동'),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.fromLTRB(10, 10, 0, 5),
                                    child: Column(
                                      children: [
                                        Image.asset(
                                          'assets/eyes.png',
                                          width: 120,
                                          height: 120,
                                        ),
                                        Text('부원: 유길동'),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ))
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
